# TP5 : A la carte

Pour ce dernier TP, on va faire un truc un poil moins guidé, mais aussi plus libre pour vous.

Vous êtes libres de choisir un sujet de votre choix, à me soumettre avant bien sûr, ou choisir l'un des trois proposés.  
Les trois touchent au réseau, mais sous un angle différent.

- [Sécurité offensive et Développement : MITM et DNS spoofing](./mitm/README.md)
- [Sécurité défensive et Admin : Serveur VPN](./vpn/README.md)
- [Réseau et Infra : VLAN et Router-on-a-Stick](./infra/README.md)
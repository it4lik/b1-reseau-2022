# Cours

Cours :

- [IP](./ip/README.md)
- [ARP](./arp/README.md)
- [DHCP](./dhcp/README.md)
- [Routage](./routage/README.md)
- [Relation client/serveur](./client_server/README.md)
- [TCP et UDP](./tcp_udp/README.md)

Mémos :

- [Rocky Linux network](./memo/rocky_network.md)
- [Install VM](./memo/install_vm.md)
- [Mémo Cisco](./memo/memo_cisco.md)

Bonus :

- [Intro chiffrement](./bonus/intro_crypto/README.md)
- [Le réseau Tor](./bonus/tor/README.md)

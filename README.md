# B1 Réseau 2022

Vous trouverez ici les ressources liées au cours de réseau de première année.

# [Cours](./cours/README.md)

- [IP](./cours/ip/README.md)
- [ARP](./cours/arp/README.md)
- [DHCP](./cours/dhcp/README.md)
- [Routage](./cours/routage/README.md)
- [Relation client/serveur](./cours/client_server/README.md)
- [TCP et UDP](./cours/tcp_udp/README.md)

# [Mémo](./cours/memo/README.md)

- [Rocky Linux network](./cours/memo/rocky_network.md)
- [Install VM](./cours/memo/install_vm.md)
- [Mémo Cisco](./cours/memo/memo_cisco.md)

# [TP](./tp/README.md)

- [TP1 : Premiers pas réseau](./tp/1/README.md)
- [TP2 : Ethernet, IP, et ARP](./tp/2/README.md)
- [TP3 : On va router des trucs](./tp/3/README.md)
- [TP4 : TCP, UDP et services réseau](./tp/4/README.md)
- [TP5 : A la carte](./tp/5/README.md)

# [Bonus](./cours/bonus/README.md)

- [Intro chiffrement](./cours/bonus/intro_crypto/README.md)
- [Le réseau Tor](./cours/bonus/tor/README.md)
